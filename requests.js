const fetch = require('node-fetch');
const R = require('ramda');
const {RETRIES, DELAY, RETRY_CODES} = require('./constants')
const { v4: uuidv4 } = require('uuid');

const fallback = () => {message:"Supply chain is down but your request has been saved."}

//local state to save the failed requests
let pendingRequests = []

const executePendingReqs = () => {
	//execute all pending requests plus any new in correct sequence
	const result = pendingRequests.reduce( (accumulatorPromise, nextReq) => {
	  return accumulatorPromise.then((res) => {
		  if(res && RETRY_CODES.includes(res.status)) throw 'Supply Chain Unreachable'
	    return fetch(nextReq.url, nextReq.options);
	  });
	}, Promise.resolve());
	
	
	return result.then(res => {
		//clear pending requests
		pendingRequests = []
		if (res.status === 204) return 
		if (res.ok ||  res.status ===404 ) return res.json()
	
	}).catch(e => {
		console.log(e);
		return fallback();
	});
	
}

const retryReg = (retries = RETRIES, delay = DELAY) => (url, options = {}) => {
	//if pending requests add the newest send them all again
	if(pendingRequests.length > 0)
	{
		pendingRequests.push({url, options})
		return executePendingReqs()
	}
	const fn = fetch(url, options)
	return fn.then( async res => {
		if (res.status === 204) return 
		if (res.ok ||  res.status ===404 ) return res.json()
			if(retries>0 && RETRY_CODES.includes(res.status))
			{
				//retry to send the request after some delay
				return new Promise(res => { setTimeout(_ => {res(retryReg(retries-1, delay*2)(url,options))}, delay) })
			}
			else{
				//if after the retries again failed to send the requests save it locally for later process
				if(!R.isEmpty(options)){
					pendingRequests.push({url, options})
				} 
				//return default fallback response
				return  fallback()
			}
				
	} ).catch(console.error)
}

const defaultRetryReg = retryReg(RETRIES, DELAY)

const getRequest = url => defaultRetryReg(url)

const delRequest = url => defaultRetryReg(url, { method: 'DELETE', })

const postRequest = ( url, payload ) => defaultRetryReg(url, {
    method: 'POST',
    body: JSON.stringify(payload),
    headers: { 'Content-Type': 'application/json' }
} )

module.exports = {getRequest, postRequest, delRequest}
