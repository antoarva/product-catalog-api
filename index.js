const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const port = 3000;
const { check, validationResult } = require('express-validator');
const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
const swaggerDocument = YAML.load('./swagger.yaml');
const {getProductList, addProduct, deleteProduct, updateProduct} = require('./service')

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/api-docs', swaggerUi.serve,swaggerUi.setup(swaggerDocument));

app.get('/products', async (req,res)=> res.send(await getProductList()))

app.post('/add', async (req,res)=> res.sendStatus(await addProduct(req.body)))

app.post('/update',check('id').exists(), async (req,res)=> {
	try{ 
		validationResult(req).throw();
		res.sendStatus(await updateProduct(req.body))
	} catch(error) {
		res.status(400).json(error)
	}

})

app.delete('/delete/:id', async (req,res)=> res.sendStatus(await deleteProduct(req.params.id)))

const server = () => {
	console.log(`Start listen at http://localhost:${port}`);
}

app.listen(port, server)

module.exports = app
