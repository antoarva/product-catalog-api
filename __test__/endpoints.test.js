const supertest = require('supertest');
const app = require('../index');
const R = require('ramda')

describe("/products", () => {

	it("GET - respond with status 200 json payload", async () => {

		const response = await supertest(app).get('/products');
		expect(response.status).toBe(200);
		expect(response.body).toBeInstanceOf(Array);

	});

	it("POST - respond with not found error", async () => {

		const response = await supertest(app).post('/products');
		expect(response.statusCode).toBe(404);

	});
});


describe("/add", () => {

	it("GET - respond with not found error", async () => {
		const response = await supertest(app).get('/add');
		expect(response.statusCode).toBe(404);
	});

	it("POST - add new product and check if added in supply chain system", async () => {
		const newProduct = {id:'supertest1', name:'supertest1',price:300, quantity:100}
		const response = await supertest(app).post('/add').send(newProduct).set('Accept', 'application/json');
		expect(response.statusCode).toBe(200);

		const resProducts = await supertest(app).get('/products');
		const bundle= resProducts.body
		const record =  R.find(R.propEq('id', newProduct.id))(bundle)
		expect(record).toBeDefined()
	});
});


describe("/update", () => {

	it("GET - respond with not found error", async () => {
		const response = await supertest(app).get('/update');
		expect(response.statusCode).toBe(404);
	});

	it("POST - repsond with bad request", async () => {
		const newProduct = { name:'supertest1',price:300, quantity:100}
		const response = await supertest(app).post('/update').send(newProduct).set('Accept', 'application/json');
		expect(response.statusCode).toBe(400);
	});

	it("POST - update existing product and check if updated in supply chain system", async () => {
		const newProduct = {id:'supertest1', name:'supertest1',price:100, quantity:100}
		const response = await supertest(app).post('/update').send(newProduct).set('Accept', 'application/json');
		expect(response.status).toBe(200);

		const resProducts = await supertest(app).get('/products');
		const bundle= resProducts.body
		const record =  R.find(R.propEq('id', newProduct.id))(bundle)
		expect(record).toBeDefined()
		expect(record.price).toBe(100)
	});
});

describe("/delete", () => {

	it("DELETE - delete product and check if removed from supply chain system", async () => {
		const response = await supertest(app).delete('/delete/supertest1');
		expect(response.statusCode).toBe(200);

		const resProducts = await supertest(app).get('/products');
		const bundle= resProducts.body
		const record =  R.find(R.propEq('id', 'supertest1'))(bundle)
		expect(record).toBeUndefined()
	});
});
