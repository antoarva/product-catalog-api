## Table of Contents

* [Goal](#goal)
* [Installation](#installation)
* [Documentation](#documentation)
* [Summary](#summary)
* [Next Steps](#next-steps)


## Goal

Create a REST API to maintain a product catalog and propagate all changes to a 3rd party supply chain system.


### Installation

Prerequisite: you will need node v12.19.0

- Clone the repo
- cd product-catalog-api

```
npm install
```

### Test

```
npm run test
```

### Start the server

```
npm run start
```

### Documentation

API hosts documentation and playground at ```/api-docs``` you can access it [here](http://localhost:3000/api-docs/){:target="_blank"}


## Summary

- **Framework:** Established express framework has being used to create our server. Express offers routing capabilities and rich enviroment of packages with community support.

- **Database:** As database NeDB has being used, which is subset of mongoDB, following the same API. NeDB is in memory database efficient enough for this type of projects and we can easily migrate it to a real database.

- **Fault-Tolerance:** To make the system more robust and ensure delivery of our changes 2 mechanisms have taken place:
	- Retry to execute the requests with timeout (default = 350ms).
	- Save all failed requests locally and call them again on the next request to avoid descrepancies between local database and supply chain
 
## Next Steps

- **Circuit Breaker:** As an improvement we can implement circuit breaker pattern for our requests. Instead of using small and transaction-specific static timeouts, we can use circuit breakers to deal with errors. They can be very useful in a distributed system where a repetitive failure can lead to a snowball effect and bring the whole system down.

- **Message Broker:** Propagate all failed requests into a message broker which is usually the backbone of real world applications

- **Security:** Improve the security of the API starting from basic login and token authentication. Integrate packages to protect HTTP headers like helmet, avoid ReDoS - CSRF attack and prevent NoSQL injections
