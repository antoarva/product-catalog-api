var Datastore = require('nedb')
  , db = new Datastore()

const {getRequest, postRequest, delRequest} = require('./requests')

const urlAPI = 'https://ev5uwiczj6.execute-api.eu-central-1.amazonaws.com/test/supply-chain'

const getProductList = () => new Promise((resolve,reject)=> {
    db.find({}, function (err, docs) {
        resolve(docs);
    });
}); 

const addProduct = product => new Promise((resolve,reject)=> {
	db.insert(product, function (err, newDocs) {
		if(newDocs)
		{
			//propagate change in supply chain
			postRequest(urlAPI, product)
		}

	const statusCode = newDocs? 200:500
	resolve(statusCode);
    });
});

const deleteProduct = id =>new Promise((resolve,reject)=> {
    db.remove({id}, function (err, numRemoved) {
	    if(numRemoved)
	    {
		//propagate deletion to supply chain
       		 delRequest(`${urlAPI}/${id}`)		    
	    }
	const statusCode = numRemoved ? 200:404
	resolve(statusCode);
    });
}); 

const getProduct = id => getRequest(`${urlAPI}/${id}`)

const updateProduct = async product => {
	const {id} = product;
	return new Promise((resolve, reject) => {
	db.update({id}, product, {}, function (err, numReplaced) {
		if(numReplaced)
		{
			//propagate update to supply chain
			postRequest(urlAPI, product)
		}
		const statusCode = numReplaced? 200:404
		resolve(statusCode)
	});

	})
	
}

module.exports = {getProductList, addProduct, deleteProduct, updateProduct}
